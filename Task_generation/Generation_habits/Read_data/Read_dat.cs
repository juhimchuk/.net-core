﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Generation_habits.Read_data
{
    public class Read_dat
    {
        public void ReadData()
        {
            string answer = "";
            
            List<Tabel> table = new List<Tabel>();
            do {
                Console.WriteLine("Please enter the data:");
                Read_week week = new Read_week();
                int weeks = week.Read_Week();

                Read_day days = new Read_day();
                int day = days.Read_Day();

                Read_habit habits = new Read_habit();
                Habits habit = habits.Read_Habit();
                
            Tabel table1 = new Tabel(weeks,day,habit);
            
            table.Add(table1);
            
                Console.Write("If you want add note to diary please click +: ");
                answer = Console.ReadLine();
                Read_screen reader = new Read_screen();
                reader.read_screen(answer);
            }
            while (answer=="+");
            XMLSerializer serializer = new XMLSerializer();
            serializer.XmlSerializer(table);
            XMLDeserializer deserializer = new XMLDeserializer();
            deserializer.XmlDeserializer();
        }
    }
}
