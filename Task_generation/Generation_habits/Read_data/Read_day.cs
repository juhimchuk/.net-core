﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generation_habits.Read_data
{
    class Read_day
    {
        public int Read_Day()
        {
            int days = 0;
            Chek_number chek = new Chek_number();
            do
            {
                Console.Write("Day number ");
                string day = Console.ReadLine();
                if (chek.chek_number(day))
                {
                    days = Convert.ToInt32(day);

                }
            }
            while (days < 1 || days > 7);
            return days;
        }
    }
}
