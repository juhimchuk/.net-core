﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generation_habits.Read_data
{
    class Read_habit
    {
        public Habits Read_Habit()
        {
            List<string> Lhabits = new List<string>();
            Console.WriteLine();
            Console.Write("List habits ");
            string habits = Console.ReadLine();
            Lhabits.AddRange(habits.Split(','));
            Habits habit = new Habits(Lhabits);
            return habit;
        }
    }
}
