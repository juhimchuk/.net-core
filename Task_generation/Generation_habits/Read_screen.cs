﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Generation_habits.Read_data;
namespace Generation_habits
{
   public class Read_screen
    {
        public void read()
        {
            Console.WriteLine(@"To write in the diary, indicate the week number, day number and list of habits that you did not observe on that day.
To start a note, enter +;
View results enter result.");
            string choice = Console.ReadLine();
            read_screen(choice);
        }
        public void read_screen(string choice)
        { 
            switch(choice)
            {
                case "+":
                    Read_dat reading = new Read_dat();
                    reading.ReadData();
                    break;
                case "result":
                    Console.WriteLine("");
                    break;
                default:
                    Console.WriteLine("No");
                    break;
            }
        }
    }
}
