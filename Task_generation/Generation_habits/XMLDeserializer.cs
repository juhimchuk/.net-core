﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Generation_habits
{
    public class XMLDeserializer
    {
        public void XmlDeserializer()
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Tabel>));
            using (FileStream fs = new FileStream("tabel.xml", FileMode.OpenOrCreate, FileAccess.Read))
            {
                List<Tabel> newtable = (List<Tabel>)formatter.Deserialize(fs);

                foreach (Tabel p in newtable)
                {
                    Console.WriteLine("Week: {0} --- Day: {1} --- Habits: {2}", p.Week, p.Day, p.Habits.ToString());
                }
            }
        }
    }
}
