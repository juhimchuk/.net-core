﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Generation_habits
{
    public class XMLSerializer
    {
        public void XmlSerializer(List<Tabel> table)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(List<Tabel>));

            using (FileStream fs = new FileStream("tabel.xml", FileMode.OpenOrCreate, FileAccess.Write))
            {
                formatter.Serialize(fs, table);
            }

           
        }
    }
}
