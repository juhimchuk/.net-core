﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractShape;

namespace BLShape
{
	public class Rectangle : Shape
	{
		private double a;
		private double b;
		public Rectangle(double a, double b)
		{
			this.a = a;
			this.b = b;

		}
		public string Information()
		{
			return "This is Rectangle";
		}
		public override double Perimetr(double a, double b, double c = 0)
		{
			double perimetr = 2 * (b + a);
			return perimetr;
		}
		public override double Square(double a, double b, double c = 0)
		{
			double square = b * a;
			return square;
		}
	}
}
