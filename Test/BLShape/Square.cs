﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLShape
{
	public class Squar : Rectangle
	{
		private double a;

		public Squar(double a, double b) : base(a, b)
		{
			this.a = a;
		}

		public string Information()
		{
			return "This is Shape";
		}
		public override double Perimetr(double a, double b, double c = 0)
		{
			double perimetr = 4 * a;
			return perimetr;
		}
		public override double Square(double a, double b, double c = 0)
		{
			double square = a * a;
			return square;
		}
	}
}
