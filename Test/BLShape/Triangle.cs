﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractShape;

namespace BLShape
{
	public class Triangle : Shape
	{
		private double a;
		private double b;
		private double c;
		double per;
		double perimetr;
		public Triangle(double a, double b, double c)
		{
			this.a = a;
			this.b = b;
			this.c = c;

		}
		public string Information()
		{
			return "This is Triangle";
		}
		public override double Perimetr(double a, double b, double c )
		{
			perimetr = c + b + a;
			return perimetr;
		}
		public override double Square(double a, double b, double c )
		{
			Triangle perimet = new Triangle(a, b,  c);
			per = perimet.Perimetr(a, b, c);
			double square = Math.Sqrt(per / 2 * ((per / 2) - a) * ((per / 2) - b) * ((per / 2) - c));
			return square;
		}
	}
}
