﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractShape;
namespace FactoryShape
{
	public abstract class Abstract_Shape
	{
		public abstract Shape CreateShape(double a, double b, double c);
	}
}
