﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractShape;
using BLShape;

namespace FactoryShape
{
	public class Factory_Shape_Triangle : Abstract_Shape
	{
		public override Shape CreateShape(double a, double b, double c )
		{
			return new Triangle(a,b,c);
		}
	}
}
