﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbstractShape;
using BLShape;
namespace FactoryShape
{
	public class Shapes
	{
		public Shape shape;
		private double a;
		private double b;
		private double c;

		public Shapes(Abstract_Shape factory)
		{
			shape =factory.CreateShape(a, b, c );
		}
		public string Info()
		{
			return shape.Information();
		}
		public double Square(double a,double b, double c=0)
		{
			return shape.Square(a,b,c);
		}
		public double Perimeter(double a, double b, double c = 0)
		{
			return shape.Perimetr(a,b,c);
		}
	}
}
