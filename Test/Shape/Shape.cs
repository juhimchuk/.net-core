﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractShape
{
   
		public abstract class Shape
		{
			private double a;
			private double b;
			public abstract double Perimetr(double a, double b, double c = 0);
			public string Information()
			{
				return "This is Shape";
			}
			public virtual double Square(double a, double b, double c = 0)
			{
				 double square = a * b;
			return square;
			}
		}
	}

