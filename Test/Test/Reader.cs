﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
	class Reader
	{
		public void Read()
		{
			 double a,b=0,c;
			Select_triangle triangl = new Select_triangle();
			Select_Rectangle rect = new Select_Rectangle();
			Select_Square sqr = new Select_Square();
			Console.Write("Please select shape (1-triangle, 2-rectangle, 3-square): ");
			string select = Console.ReadLine();

			switch (select)
			{
				case "1":
					Console.Write("Input first side :");
					a = Convert.ToInt32(Console.ReadLine());
					Console.Write("Input second side :");
					b = Convert.ToInt32(Console.ReadLine());
					Console.Write("Input third side :");
					c = Convert.ToInt32(Console.ReadLine());
					triangl.select_triangle(a,b,c);
					break;
				case "2":
					Console.Write("Input first side :");
					a = Convert.ToInt32(Console.ReadLine());
					Console.Write("Input second side :");
					b = Convert.ToInt32(Console.ReadLine());
					rect.select_rectangle(a, b);
					break;
				case "3":
					Console.Write("Input side :");
					a = Convert.ToInt32(Console.ReadLine());
					sqr.select_square(a, b);
					break;
				default:
					break;
			}
			
		}
	}
}
