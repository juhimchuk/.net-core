﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryShape;

namespace Test
{
	class Select_Rectangle
	{
		public void select_rectangle(double a, double b)
		{
			Shapes rectangle = new Shapes(new Factory_Shape_Rectangle());
			Console.WriteLine(rectangle.Info());
			Console.WriteLine("Perimetr ---{0}",rectangle.Perimeter(a, b));
			Console.WriteLine("Square ---{0}", rectangle.Square(a, b));
		}
	}
}
