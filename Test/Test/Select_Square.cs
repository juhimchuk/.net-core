﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryShape;


namespace Test
{
	class Select_Square
	{
		public void select_square(double a, double b)
		{
			Shapes sqr = new Shapes(new Factory_Shape_Square());
			Console.WriteLine(sqr.Info());
			Console.WriteLine("Perimetr ---{0}",sqr.Perimeter(a, b));
			Console.WriteLine("Square ---{0}",sqr.Square(a, b));
		}
	}
}
