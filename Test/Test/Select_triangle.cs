﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FactoryShape;

namespace Test
{
	class Select_triangle
	{
		public void select_triangle(double a, double b, double c)
		{
			Shapes triangle = new Shapes(new Factory_Shape_Triangle());
			Console.WriteLine(triangle.Info());
			Console.WriteLine("Perimetr ---{0}",triangle.Perimeter(a,b,c));
			Console.WriteLine("Square ---{0}", triangle.Square(a,b,c));
		}
	}
}
